.. title: Download
.. slug: download
.. date: 2016-12-03 10:21:31 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

Installing openATTIC
--------------------

openATTIC can be installed on Linux only.

There is no individual packaged download of the software - we provide dedicated
package repositories to facilitate the easy installation and automatic updating
using the Linux distribution's built-in package management.

Packages for SUSE Linux (openSUSE Leap) can be obtained from
the `openSUSE Build Service <http://software.opensuse.org/>`_.

See the `openATTIC Installation and Getting Started Guide
<http://docs.openattic.org/en/latest/install_guides/>`_ for instructions on how to
subscribe to these repositories in order to install these packages using your
distribution's package management utilities.