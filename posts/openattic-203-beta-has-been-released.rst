.. title: openATTIC 2.0.3 beta has been released
.. slug: openattic-203-beta-has-been-released
.. date: 2015-10-16 19:16:41 UTC+02:00
.. tags: news, beta, announcement
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Lenz Grimmer

We're happy to announce that openATTIC version 2.0.3 beta has now been
released!

We're trying to stick to the core mantra of Open Source, by releasing early
and often. We intend to establish a cadence of publishing a new release build
roughly every 5-6 weeks. For the impatient, we also provide nightly builds
that contain all changes that have been committed into the development branch
and have passed the test suites.

One notable highlight in the 2.0.3 release is the inclusion of some additional
Ceph support in the Web UI: it is now possible to review the `CRUSH map
<http://docs.ceph.com/docs/master/rados/operations/crush-map/>`_ and perform
some basic editing of the Ceph cluster layout (replication rules, placements)
this way. We're very eager on receiving feedback on this functionality, as
it's currently still under development.

We've also made some progress in adding DRBD support to the openATTIC RESTful
API - it's now possible to create and delete mirrored volumes. However, web
UI support and a method to resize existing mirrored volumes is still in the
works - we hope to deliver these missing pieces with the next release.

In addition to these new features, we fixed several bugs and usability issues.
We also added numerous additional tests to the API and UI test suites.

Thanks to everyone who contributed to this release!

.. TEASER_END

We hope you will give this new version a try. Please note that this is still a
**beta version** - handle with care and make sure to create backups of your
data.

As usual, feedback is very welcome. If you would like to get in touch with us,
consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, or
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_.

Here's a more detailed changelog - the OP codes in brackets refer to
individual Jira issues that provide additional details on each item. You can
review these on our `public Jira instance <http://tracker.openattic.org/>`_.

* API/Backend: Created DRBD REST-API that supports creating and deleting
  mirrored volumes (:issue:`OP-216`).
* Web UI: Merged the Ceph Enterprise UI components (CRUSH map editing) into the
  main repo (:issue:`OP-675`).
* Web UI: The openATTIC login screen now automatically places the input focus
  on the username input field, adjusted the login icon width (:issue:`OP-688`, :issue:`OP-689`)
* Web UI: Created E2E tests for login with new created user account (:issue:`OP-629`),
  extended and added numerous additional E2E tests.
* Web UI: Clicking the Delete button with no host selected no longer triggers a
  delete action (:issue:`OP-683`)
* Installation: Fixed lots of "user/group does not exist" warnings that ocurred
  when installing the EL7 RPMs (:issue:`OP-536`)
* Installation: Moved log files into /var/log/openattic and created a
  logrotate.conf file (:issue:`OP-660`, :issue:`OP-661`)
* Installation: Removed /var/log/openattic_vgmanager from the Debian packages
  (:issue:`OP-663`)
* Installation: Fixed RPM dependencies: all modules depend on "openattic-base"
  now, removed openattic-base dependency from the "openattic" meta package.
* Documentation: Updated the installation and development system setup chapters.
