.. title: openATTIC 2.0.2 beta has been released
.. slug: openattic-202-beta-has-been-released
.. date: 2015/09/10 15:00:00
.. tags: news, beta, announcement
.. category: 
.. link:
.. description: Announcing the release of openATTIC 2.0.2 beta
.. type: text
.. author: Lenz Grimmer

It is our great pleasure to announce the immediate availability of openATTIC
2.0.2 beta! Since our last public beta release (2.0.1) that we `announced
<http://www.code-rausch.com/openattic-2-0-1-beta-has-been-released/>`_ on July
21st, 2015, the team has made some notable progress.

While not immediately visible to end-users, a lot of work went into
restructuring and reorganizing the openATTIC code base itself. We merged
several formerly separate Mercurial repositories into the main openattic
repository and deleted obsolete files.

.. TEASER_END

This change will allow us to become more agile and to better work on new
features in separate branches of the same repository. It also makes it easier
for external contributors; now the entire code base including the new Web UI,
documentation and API/UI test suites are part of a single Mercurial
repository. The implementation of new features can now include the respective
updates to the documentation as well as related QA tests in the same change
set.

Speaking of documentation: the `current documentation for version 2.0
<http://docs.openattic.org/2.0/>`_ is still work in progress. We've decided to
start from scratch, beginning with an "Installation and Getting Started" guide
to get you going. Other chapters dedicated to the new web UI or the new REST
API will be added over time, merging content from the `1.2 documentation
<http://docs.openattic.org/1.2/>`_ where appropriate.

This release also includes a new Ceph module (package name
``openattic-module-ceph`` that provides preliminary support for creating Ceph
RADOS block devices (RBDs) in a Ceph cluster. Such a device can then be
treated as any other block device, e.g. creating a file system on it and
sharing it via CIFS, NFS or iSCSI.

We also fixed several issues in the Web UI, adding some missing functionality
and making usability improvements.

Additionally, the RPM packages for RHEL 7 and derivative distributions
received a major overhaul and now include an ``openattic-release`` package, to
simplify the installation from our public yum repository at
http://repo.openattic.org/. Similar to the Debian/Ubuntu packages, the RPMs
are now signed with our GPG build key and we provide both official releases as
well as nightly builds from two separate yum repositories.

We would like to thank Felix Netzel and Thomas Schweikle for reporting several
of the issues that were fixed for this release.

We hope you give this new version a try! Please note that this is still a
**beta version** - handle with care and make sure to create backups of your
data.

As usual, feedback is very welcome. If you would like to get in touch with us,
consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, or
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_.

A more detailed changelog follows:

* API: Merged the formerly separate REST API test suite into the openATTIC
  code base (subdirectory ``gatling``).
* Documentation: Created an initial version of the 2.0 documentation (derived
  from the 1.2 docs) and added it to the openATTIC code base (subdirectory
  ``documentation``) (:issue:`OP-502`).
* Installation: Added ``--allow-broken-hostname`` option to allow ``oaconfig
  install`` to proceed without a correctly-configured FQDN (:issue:`OP-501`).
* Installation: Added Ceph module and created ``openattic-module-ceph``
  installation package (:issue:`OP-623`, :issue:`OP-624`).
* Installation: created ``openattic-release`` RPM package, to simplify the
* installation process (:issue:`OP-537`).
* Installation: Fixed a missing ``python-djangorestframework`` dependency for
  Ubuntu 14.04 (:issue:`OP-510`).
* Installation: Fixed ``oaconfig`` failure when ``/etc/aliases`` is missing
  (thanks to Thomas Schweikle for reporting).
* Installation: Fixed RPM installation error (``semanage: command not found``)
  (:issue:`OP-571`).
* Installation: Fixed ``/var/lib/openattic/ftp does not exist`` warning during
  the installation (:issue:`OP-508`).
* Installation: Overhauled the EL7 RPM packages.
* Installation: RPM packages are now signed with GPG (:issue:`OP-573`).
* WebUI: Added missing Volume Resize option (:issue:`OP-425`).
* WebUI: Added performance graph to the Dashboard.
* WebUI: Adding iSCSI LUNs now only works for hosts that have iqn/wwn
  attributes (:issue:`OP-469`).
* Web UI: Fixed a login problem with a newly created user (:issue:`OP-615`, thanks to
  Felix Netzel for reporting).
* WebUI: Fixed broken Disk Details URL on the Disk Management page (:issue:`OP-540`).
* Web UI: Fixed iSCSI LUNs not deletable (:issue:`OP-618`, thanks to Felix Netzel for
  reporting).
* Web UI: Fixed some UI usability issues, e.g. "When deleting a volume,
  pressing "Enter" after confirming the volume name cancels the delete action"
  (:issue:`OP-565`), "Pressing "Enter" in the volume resize dialogue cancels the
  intended action" (:issue:`OP-601`).
* WebUI: Heavily extended the UI test suite.
* Web UI: Merged the formerly separate Web UI into the openATTIC code base
  (subdirectory ``webui``).
* WebUI: Unified the types of the checkboxes of the cluster status widget
  (:issue:`OP-566`).
