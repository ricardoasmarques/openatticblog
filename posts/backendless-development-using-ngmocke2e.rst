.. title: Backend-less development using ngMockE2E
.. slug: backendless-development-using-ngmocke2e
.. date: 2017-04-24 11:16:56 UTC+01:00
.. tags: contributing, community, development, angular
.. category:
.. link:
.. description: How to use ngMockE2E to develop a UI prototype before backend implementation
.. type: text
.. author: Ricardo Marques

There are several reasons why you may need to mock HTTP requests. The most common are the unit tests, but imagine
that you want to start developing a UI prototype before you have the backend implementation, in this case,
`ngMockE2E <https://docs.angularjs.org/api/ngMockE2E/>`_ can help.

Here's what you should do in **openATTIC** if you want to mock an HTTP request to develop a UI prototype.

.. TEASER_END

1. Add the `"angular-mocks"` dependency to the `"bower.json"` file (use the same version as the `"angular"`)::

    {
      ...
      "dependencies": {
        "angular": "1.5.3",
        ...
        "angular-mocks": "1.5.3",
        ...
      },
      ...
    }

2. Import `"angular-mocks"` on the `"index.tpl.html"` file::

    <script src="bower_components/angular-mocks/angular-mocks.js"></script>

3. Create `"webui/app/scripts/module_openattic-mocks.js"` file::

    "use strict";

    var app = angular.module("openattic.mocks", [
      "ngMockE2E"
    ]);
    app.run(function ($httpBackend) {

      // Your mocks are defined here
      // This is a simple example
      $httpBackend.whenGET(/^.*\/ceph_deployment\/.*\/iscsitargets.*$/).respond(200, {
        count: 1,
        next: null,
        previous: null,
        results: [
          {
            id: 1,
            targetId: "iqn.2016-06.org.openattic:storage:disk.sn-a8675309",
            nodes: [
              {
                hostname: "node1",
                interface: "192.168.100.201"
              },
              {
                hostname: "node2",
                interface: "192.168.100.202"
              }
            ],
            images: [
              {
                name: "img-foo1",
                pool: "pool-rbd1"
              },
              {
                name: "img-foo2",
                pool: "pool-rbd1"
              }
            ]
          }
        ]
      });

      // Other requests should pass through the real $httpBackend
      $httpBackend.whenGET(/^.*$/).passThrough();
      $httpBackend.whenHEAD(/^.*$/).passThrough();
      $httpBackend.whenDELETE(/^.*$/).passThrough();
      $httpBackend.whenPOST(/^.*$/).passThrough();
      $httpBackend.whenPUT(/^.*$/).passThrough();
      $httpBackend.whenPATCH(/^.*$/).passThrough();

    });

4. Add your `"openattic-mocks"` as a dependency of `"openattic"` module (`"webui/app/scripts/module.js"` file)::

    angular.module("openattic", [
        ...
        "openattic.mocks"
    ]);

Now your HTTP request is mocked, and you can start developing your UI prototype!
