.. title: Sneak preview of the openATTIC GUI redesign
.. slug: sneak-preview-of-the-openattic-gui-redesign
.. date: 2016-04-01 14:22:05 UTC+02:00
.. tags: development, beta, gui
.. category:
.. link:
.. description: Preview of the newly designed openATTIC Web UI
.. type: text
.. author: Lenz Grimmer

Still work in progress, but we'd like to share some preview screen shots of an
upcoming redesign of the openATTIC 2.0 web interface.

The redesign was necessary after we decided to drop the "smartadmin" bootstrap
template. The update is almost done, so stay tuned for the inclusion in a
future release!

.. slides::

  /galleries/oA-GUI-newlayout-2016-04/oA-newlayout-login.png
  /galleries/oA-GUI-newlayout-2016-04/oA-newlayout-dashboard.png
  /galleries/oA-GUI-newlayout-2016-04/oA-newlayout-disks.png
  /galleries/oA-GUI-newlayout-2016-04/oA-newlayout-pools.png
  /galleries/oA-GUI-newlayout-2016-04/oA-newlayout-volumes.png

We hope you like the new layout!
