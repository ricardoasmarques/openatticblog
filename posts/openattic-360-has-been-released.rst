.. title: openATTIC 3.6.0 has been released
.. slug: openattic-360-has-been-released
.. date: 2017-11-01 21:48:03 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release 
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.6.0 release
.. type: text
.. author: Kai Wagner

We're happy to announce version 3.6.0 of openATTIC. 

Given the fact that openATTIC 3.5.3 was only a bug fix release this 3.6.0 
release includes all the improvements and changes since 3.5.2. We cleaned up and 
removed a lot of unnecessary things and also made some usability improvements
to the UI.
 
The most visible change in openATTIC 3.6.0 is the unification of Grafana and
our "old" widgets into one single dashboard page.
    
We now have one unified dashboard and you don't have to switch between the "old"
and the new Grafana dashboard. We included the functionality of the old status 
widget into our default dashboard. If your cluster is in an error or warning 
state, this will be shown at the top of your dashboard including the failure 
message from the 'ceph health' command. 

.. TEASER_END

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.6.0
---------------------------
    
Added
~~~~~
* Backend: The error string of a failed call in an external process does now
  include the command name (:issue:`OP-2780`)
* WebUI: Prevent the removal of RGW user that is in use on the settings
  page. (:issue:`OP-2737`)
* WebUI: Unit tests for IQN validator (:issue:`OP-2783`)
* WebUI: Usage of "unsaved changes" dialog on iSCSI form (:issue:`OP-2254`)
* WebUI: Add HTML linter with a few new rules (:issue:`OP-2544`)
* WebUI: Add fast-diff help text used in RBD form (:issue:`OP-2767`)
* Documentation: Added our line length PEP-8 exception to the backend
  contributing guidelines (:issue:`OP-823`)
* WebUI: Add no-extra-semi rule to eslint (:issue:`OP-2844`)
    
Changed
~~~~~~~
* Backend: Cleaned up pep8 warnings of ceph app (:issue:`OP-1502`)
* Backend: Cleaned up pep8 warnings of nodb app (:issue:`OP-1512`)
* Backend: Cleaned up pep8 warnings of rest app (:issue:`OP-1515`)
* Backend: Cleaned up pep8 warnings of systemd app (:issue:`OP-1518`)
* Backend: Use health's "status" instead of "overall_status" (:issue:`OP-2845`)
* WebUI: Display dates in locale date format. All Django DateTimeFields will
  be returned as ISO 8601 (:issue:`OP-591`)
* WebUI: Replace babel-preset-es2015 with babel-preset-env (:issue:`OP-2772`)
* WebUI: Submit buttons should be enabled on invalid forms (:issue:`OP-2736`)
* WebUI/Backend: Salt-API settings should be optional (:issue:`OP-2722`)
* WebUI: Prevent modal from closing when clicking on the backdrop (:issue:`OP-2771`)
* WebUI/QA: Refactored unit test setup (:issue:`OP-2827`)
* WebUI/QA: Update Karma config with Cobertura report and ChromeHeadless
  (:issue:`OP-2725`)
* WebUI: Don't allow performing a delete action on buckets shared via
  NFS (:issue:`OP-2768`)
* WebUI: Improve eslint rules (:issue:`OP-2749`)
* WebUI: Now it is possible to delete a RGW user with buckets (:issue:`OP-2790`)
* WebUI: Migration of Ceph Pools controllers to components (:issue:`OP-2399`)
* WebUI: Migration of Ceph OSDs controllers to components (:issue:`OP-2401`)
* Documentation: Mentioned the AngularJS style guide as the coding style
  for frontend development (:issue:`OP-824`)
* WebUI: Migration of Ceph Nodes controllers to components (:issue:`OP-2398`)
* WebUI: Set the minimum pg size to 1 (:issue:`OP-2875`)
* WebUI: Convert Shared module to ES6 (:issue:`OP-2532`)
* WebUI: Unify Grafana and Widgets in a single Dashboard page (:issue:`2754`)
* WebUI: Select the NFS Host automatically, if only one is available (:issue:`OP-2711`)
* WebUI/QA: Optimize "ceph_pool_creation" e2e test suite (:issue:`OP-2812`)
* WebUI: Guide user through RBD creation for iSCSI (:issue:`OP-2181`)
* WebUI: Migration of Ceph RGW controllers to components (:issue:`OP-2397`)
* WebUI: Migration of Auth controllers to components (:issue:`OP-2801`)
* WebUI: Convert Settings components to ES6 (:issue:`OP-2806`)
* WebUI: Convert Grafana components to ES6 (:issue:`OP-2805`)
* WebUI: Convert Ceph RBD components to ES6 (:issue:`OP-2803`)
* WebUI: Convert iSCSI components to ES6 (:issue:`OP-2802`)
* WebUI: Migration of Api Decorator module into Angular (:issue:`OP-2850`)
* WebUI: Migration of Grafana module into Angular (:issue:`OP-2863`)
* WebUI: Migration of Ceph Cluster module into Angular (:issue:`OP-2852`)
* WebUI: Migration of ApiRecorder module into Angular (:issue:`OP-2848`)
* WebUI: Migration of Registry module into Angular (:issue:`OP-2866`)
    
Fixed
~~~~~
* Backend: Fixed Settings page doesn't save new keyring information properly
  (:issue:`OP-2729`)
* Backend: The currently logged in user can't delete himself anymore (:issue:`OP-2738`)
* Backend: Fixed `CephCluster` to return the same `keyring_candidates` twice
  (:issue:`OP-2782`)
* Backend/QA: Added missing license header in userprefs/tests.py (:issue:`OP-2769`)
* Backend: Fixed "No SystemD in ifconfig.systemapi" Message (:issue:`OP-2774`)
* Backend: Fixed db corruption when running `oaconfig install` (:issue:`OP-2784`)
* Backend: Django SECRET_KEY generated using insufficient entropy (:issue:`OP-2874`)
* Backend: The Grafana dashboard will now be shown automatically when
  upgrading from oA 2.x to 3.x (:issue:`OP-2843`)
* Development: Lint unit test correctly (:issue:`OP-2828`)
* WebUI: Allow users to type in any size into the RBD form (:issue:`OP-2730`)
* WebUI/QA: RGW bucket "e2e-rgw-add" is not deleted after RGW suite
  run (:issue:`OP-2512`)
* WebUI/QA: Prevent random failure in "2 failed task" test (:issue:`OP-2735`)
* WebUI/QA: Fix random NFS e2e test failures (:issue:`OP-2762`)
* WebUI/QA: Fix style issues in protractor.conf.js (:issue:`OP-2793`)
* WebUI/QA: Prevent random failures in task queue dialog suite (:issue:`OP-2779`)
* WebUI: Fix the indentation in pool list and form template (:issue:`OP-2829`)
* WebUI: Fix "use max" links in RBD form (:issue:`OP-2727`)
* WebUI/QA: Fix random iSCSI e2e test failures (:issue:`OP-2807, OP-2830`)
* WebUI/QA: Improve e2e testing when running in a slower system (:issue:`OP-2831`)
* WebUI: Fix RGW name validation (:issue:`OP-2825`)
* Deployment: Add "PreReq: %fillup_prereq" to SUSE spec file (:issue:`OP-2808`)
    
Removed
~~~~~~~
* Backend: Removed `utilities.in_unittest` (:issue:`OP-2776`)
* WebUI: Removed the need to type "yes" to delete tasks (:issue:`OP-2770`)
