.. title: KVM guest with acpid installed will not shutdown
.. slug: kvm-guest-with-acpid-installed-will-not-shutdown
.. date: 2016-08-30 16:17:42 UTC+02:00
.. tags: kvm, acpid, config
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Yesterday a colleague migrated a physical machine into a kvm vm. Afterwards we wanted to manage the vm within virt-manager. 

Acpid was installed, but nothing happend if we tried to shutdown or reboot the vm via acpi requests.

The problem was, that the migrated kvm vm still tought that it is a hardware instead of a vm. Therefore I changed the entry within the acpi events.

	* Edit /etc/acpi/events/powerbtn to contain action=/sbin/poweroff

As an alternative you could purge and reinstall the acpid package.

	* apt-get purge acpid
	* apt-get install acpid

Sometimes it's that easy :-)
